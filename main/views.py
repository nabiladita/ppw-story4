from django.shortcuts import render


def home(request):
    return render(request, 'main/home.html')

def aboutme(request):
    return render(request, 'main/aboutme.html')

def hobbies(request):
    return render(request, 'main/hobbies.html')

def experiences(request):
    return render(request, 'main/experiences.html')

def contact(request):
    return render(request, 'main/contact.html')
